/**
 * Created by Dmytro TOLSTYI on 09/02/2017.
 */
var {defineSupportCode} = require('cucumber');

defineSupportCode(function({After}) {
  After(function() {
    return this.driver.quit();
  });
});