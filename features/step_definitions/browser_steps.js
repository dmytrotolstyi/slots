/**
 * Created by Dmytro TOLSTYI on 09/02/2017.
 */
const webdriver = require('selenium-webdriver');
const By = webdriver.By;
const {defineSupportCode} = require('cucumber');
const expect = require('chai').expect;

defineSupportCode(function({Given, When, Then}) {
  Given('I am on the Start Page', function() {
    return this.driver.get('http://localhost:5000');
  });

  Given('Slot Machine is spinning', function() {
    const id = 'spin';
    return this.driver.findElement({id: id}).then((element) => {
      return element.click();
    })
  });

  When('I click on {stringInDoubleQuotes} button', function(text) {
    const xPath = '//button[contains(text(),"' + text + '")]';
    return this.driver.findElement({xpath: xPath}).then((element) => {
      return element.click();
    });
  });

  Then('I should see {stringInDoubleQuotes} at the header', function(text) {
    const cssSelector = 'body > header';
    return this.driver.findElement(By.css(cssSelector)).then((element) => {
      const condition = webdriver.until.elementTextIs(element, text);
      return this.driver.wait(condition, 5000);
    });
  });

  Then('I should see {stringInDoubleQuotes} button', function(text) {
    const xPath = '//button[contains(text(),"' + text + '")]';
    var condition = webdriver.until.elementLocated({xpath: xPath});
    return this.driver.wait(condition, 5000);
  });

  Then('Slot Machine should be spinning', function() {
    const id = 'slots-machine';
    return this.driver.findElement({id: id}).getAttribute('class').then((classes) => {
      expect(classes.split(' ')).to.contain('spinning');
    });
  });

  Then('Slot Machine should not be spinning', function() {
    const id = 'slots-machine';
    return this.driver.findElement({id: id}).getAttribute('class').then((classes) => {
      expect(classes.split(' ')).not.to.contain('spinning');
    });
  });
});