Feature: Slots Machine
  As a user of Slots Test Application
  I want to have control buttons of the slots machine
  So that I can execute different machine actions

  Scenario: Showing Spin button
    Given I am on the Start Page
    Then I should see "Spin" button

  Scenario: Spinning slots
    Given I am on the Start Page
    When I click on "Spin" button
    Then Slot Machine should be spinning

  Scenario: Showing Stop button
    Given I am on the Start Page
    Then I should see "Stop" button

  Scenario: Stopping slots
    Given I am on the Start Page
    And Slot Machine is spinning
    When I click on "Stop" button
    Then Slot Machine should not be spinning