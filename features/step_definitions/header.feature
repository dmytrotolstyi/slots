Feature: Page Header
  As a user of Slots Test Application
  I want to see invitation message at the header of the start  in upper case
  So that I can understand the purpose of the site

  Scenario: Reading header
    Given I am on the Start Page
    Then I should see "TEST YOUR LUCK" at the header