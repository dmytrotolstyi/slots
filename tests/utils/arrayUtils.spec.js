/**
 * Created by Dmytro TOLSTYI on 09/02/2017.
 */
const expect = require('chai').expect;

import ArrayUtils from '../../src/utils/arrayUtils.js';

describe('ArrayUtils', () => {
  describe('allEqual()', () => {
    it('should return true if all elements are equal', () => {
      const array = [1, 1, 1, 1];
      expect(ArrayUtils.allEqual(array)).to.be.true;
    });
    it('should return false if all elements are not equal', () => {
      const array = [1, 2, 1, 1];
      expect(ArrayUtils.allEqual(array)).to.be.false;
    });
  });
});