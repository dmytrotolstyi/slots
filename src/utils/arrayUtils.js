/**
 * Created by Dmytro TOLSTYI on 09/02/2017.
 */
/**
 * Contains utility methods to work with arrays
 */
export default class ArrayUtils {

  /**
   * Checks if all the items in array are equal
   * @param array
   * @return {boolean} "true" if all items are equal, "false" - otherwise
   */
  static allEqual(array) {
    return array.every((element, index, arr) => {
      return element === arr[0]
    })
  }
}
