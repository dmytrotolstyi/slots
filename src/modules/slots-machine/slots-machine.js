/**
 * Created by Dmytro TOLSTYI on 09/02/2017.
 */
const jQuery = require('jquery');

require('../../assets/scripts/jquery.slotmachine.min');
require('../../assets/styles/jquery.slotmachine.min.css');

import ArrayUtils from '../../utils/arrayUtils.js';

const LINE_SELECTOR = '.slots-line';
const RESULT_SELECTOR = '.result-container .result';

const DEFAULT_TEXT = 'Press "Spin" Button to Start';
const SPINNING_TEXT = 'Press "Stop" To Check Result';
const LOOSE_TEXT = 'Unfortunately, you lost. Try again';
const WIN_TEXT = 'Wooohooo! You won! Gratz!';

/**
 * Performs initialization and control over the Slots Machine instance.
 * Each Slot Machine can contain multiple lines of slots.
 */
export default class SlotsMachine {
  constructor(container) {
    this.container = container;
    this.lines = [];

    // Initializing each line as a separate slot machine
    jQuery(container).find(LINE_SELECTOR).each((key, value) => {
      this.lines.push(jQuery(value).slotMachine());
    });
    this._setResultText(DEFAULT_TEXT);

  }

  /**
   * Spins all the lines of the slot machine
   */
  spin() {
    for (let line of this.lines) {
      line.shuffle();
    }
    this._setResultText(SPINNING_TEXT);
    jQuery(this.container).addClass('spinning');
  }

  /**
   * Stops all the lines of the slot machine and returns results
   *
   * @return {array} ids of the selected slots (e.g. [3,1,4])
   */
  stop() {
    for (let line of this.lines) {
      line.stop();
    }

    jQuery(this.container).removeClass('spinning');

    // Checking if user has won or not
    if (this._isWin()) {
      this._setResultText(WIN_TEXT)
    } else {
      this._setResultText(LOOSE_TEXT);
    }

    return this.getResults();
  }

  /**
   * Checks if specified line is still running
   *
   * @param lineNumber
   * @return {boolean} - "true" if line is active, "false" - otherwise
   * @throws {Error} if specified lineNumber is out of bounds of lines array
   */
  isLineRunning(lineNumber) {
    if (lineNumber >= 0 && lineNumber <= this.lines.length) {
      return this.lines[lineNumber].running;
    } else {
      throw new Error("Specified lineNumber is out of the range");
    }
  }

  /**
   * Checks if specified line is stopping
   *
   * @param lineNumber
   * @return {boolean} - "true" if line is active, "false" - otherwise
   * @throws {Error} if specified lineNumber is out of bounds of lines array
   */
  isLineStopping(lineNumber) {
    if (lineNumber >= 0 && lineNumber <= this.lines.length) {
      return this.lines[lineNumber].stopping;
    } else {
      throw new Error("Specified lineNumber is out of the range");
    }
  }

  /* Returns current combination of selected slots ids
  *
  * @return {array} ids of the selected slots (e.g. [3,1,4])
  */
  getResults() {
    let results = [];

    for (let line of this.lines) {
      results.push(line.active);
    }

    return results;
  }

  /**
   * Places specified text into machine's result section on UI
   *
   * @param text
   * @private
   */
  _setResultText(text) {
    jQuery(this.container).find(RESULT_SELECTOR).text(text);
  }


  /**
   * Checks whether current combination of slots is a win combination
   *
   * @return {boolean}
   * @private
   */
  _isWin() {
    const results = this.getResults();
    return ArrayUtils.allEqual(results);
  }
}