/**
 * Created by Dmytro TOLSTYI on 09/02/2017.
 */

const jQuery = require('jquery');

require('./index.scss');
import SlotsMachine from './modules/slots-machine/slots-machine.js';

const slotMachine = new SlotsMachine('#slots-machine');

// Spin button click handler
jQuery('#spin').click(() => {
  slotMachine.spin();
});

// Stop button click handler
jQuery('#stop').click(() => {
  console.log(slotMachine.stop());
});